import React from "react";
import A from "next/link";
import PropTypes from "prop-types";

function NavigationSidebar({ activeTitle: title }) {
  return (
    <aside className="uk-width-1-4 uk-visible@m tm-aside-column">
      <section
        className="uk-card uk-card-default uk-card-small"
        uk-sticky="offset: 90; bottom: true;"
      >
        <nav>
          <ul className="uk-nav uk-nav-default tm-nav">
            <li className={title === "About" ? "uk-active" : undefined}>
              <A href="/about">
                <a>About</a>
              </A>
            </li>
            <li className={title === "Contacts" ? "uk-active" : undefined}>
              <A href="/contacts">
                <a>Contacts</a>
              </A>
            </li>
            <li className={title === "Blog" ? "uk-active" : undefined}>
              <A href="/blog">
                <a>Blog</a>
              </A>
            </li>
            <li className={title === "News" ? "uk-active" : undefined}>
              <A href="/news">
                <a>News</a>
              </A>
            </li>
            <li className={title === "FAQ" ? "uk-active" : undefined}>
              <A href="/faq">
                <a>FAQ</a>
              </A>
            </li>
            <li className={title === "Delivery" ? "uk-active" : undefined}>
              <A href="/delivery">
                <a>Delivery</a>
              </A>
            </li>
          </ul>
        </nav>
      </section>
    </aside>
  );
}

NavigationSidebar.propTypes = {
  activeTitle: PropTypes.string.isRequired
};

export default NavigationSidebar;
