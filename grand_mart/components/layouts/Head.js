import React from "react";
import "../../public/styles/style.css";
import "react-notifications-component/dist/theme.css";
import NextHead from "next/head";

function Head() {
  return (
    <NextHead>
      <title>GrandMart. Shop</title>
      <meta charSet="UTF-8" />
      <meta httpEquiv="x-ua-compatible" content="ie=edge" />
      <meta name="viewport" content="width=device-width, initial-scale=1" />
      <link
        rel="stylesheet"
        href="//fonts.googleapis.com/css?family=Roboto:400,500"
      />
    </NextHead>
  );
}

export default Head;
