import React from "react";
import StaticPage from "../components/layouts/StaticPage";

const News = () => (
  <StaticPage title="News" hasNavigation hasTitle>
    <section className="uk-width-1-1 uk-width-expand@m">
      <section className="uk-card uk-card-default uk-card-small uk-card-body tm-ignore-container">
        <ul className="uk-list uk-list-large uk-list-divider">
          <li>
            <article className="uk-article">
              <div className="uk-article-body">
                <div className="uk-article-meta uk-margin-xsmall-bottom">
                  <time>June 4, 2018</time>
                </div>
                <div>
                  <h3>
                    <a className="uk-link-heading" href="article.html">
                      Highlights from WWDC
                    </a>
                  </h3>
                </div>
                <div className="uk-margin-small-top">
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Proin sodales eget ipsum id aliquam. Nam consectetur
                    interdum nibh eget sodales. Cras volutpat efficitur ornare.
                  </p>
                </div>
              </div>
            </article>
          </li>
          <li>
            <article className="uk-article">
              <div className="uk-article-body">
                <div className="uk-article-meta uk-margin-xsmall-bottom">
                  <time>June 4, 2018</time>
                </div>
                <div>
                  <h3>
                    <a className="uk-link-heading" href="article.html">
                      Apple introduces macOS Mojave
                    </a>
                  </h3>
                </div>
                <div className="uk-margin-small-top">
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Proin sodales eget ipsum id aliquam. Nam consectetur
                    interdum nibh eget sodales. Cras volutpat efficitur ornare.
                  </p>
                </div>
              </div>
            </article>
          </li>
          <li>
            <article className="uk-article">
              <div className="uk-article-body">
                <div className="uk-article-meta uk-margin-xsmall-bottom">
                  <time>May 29, 2018</time>
                </div>
                <div>
                  <h3>
                    <a className="uk-link-heading" href="article.html">
                      iOS 11.4 brings stereo pairs and multi-room audio with
                      AirPlay 2
                    </a>
                  </h3>
                </div>
                <div className="uk-margin-small-top">
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Proin sodales eget ipsum id aliquam. Nam consectetur
                    interdum nibh eget sodales. Cras volutpat efficitur ornare.
                  </p>
                </div>
              </div>
            </article>
          </li>
        </ul>
      </section>
    </section>
  </StaticPage>
);

export default News;
