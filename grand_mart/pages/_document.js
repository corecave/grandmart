// _document is only rendered on the server side and not on the client side

import Document, { Html, Head, Main, NextScript } from "next/document";
class MyDocument extends Document {
  static async getInitialProps(ctx) {
    const initialProps = await Document.getInitialProps(ctx);
    return { ...initialProps };
  }

  render() {
    return (
      <Html>
        <Head />
        <body>
          <NextScript />
          <Main />
          <script src="/scripts/uikit.js"></script>
          <script src="/scripts/uikit-icons.js"></script>
          <script src="/scripts/script.js"></script>
          <script
            src="//maps.googleapis.com/maps/api/js?key=AIzaSyClyjCemJi4m2q78gNeGkhlckpdH1hzTYA&amp;callback=initMap"
            async
            defer
          ></script>
        </body>
      </Html>
    );
  }
}

export default MyDocument;
