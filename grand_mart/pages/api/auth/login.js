import bcrypt from "bcrypt";
import { User } from "~/models";
import { generateJWTForUser } from "~/helpers/token";

export default (req, res) => {
  if (req.method === "POST") {
    const { email, password } = req.body;
    User.findOne({
      where: { email },
      attributes: ["id", "email", "password"]
    })
      .then(async user => {
        if (!user) {
          res.status(401).json({ error: "Invalid login credentials." });
          return;
        }

        const passed = await bcrypt.compare(password, user.password);

        if (!passed) {
          res.status(401).json({ error: "Invalid login credentials." });
          return;
        }

        return user;
      })
      .then(async user => {
        if (user) {
          req.session.authorization = await generateJWTForUser(user);

          return res.json({
            success: "You logged in successfully. Please wait page refresh..."
          });
        }
      });
  } else {
    return res.status(400).json({ error: "400 Bad Request" });
  }
};

export const config = {
  api: {
    bodyParser: false
  }
};
