const { Router } = require("express");
const auth = require("./auth");
const session = require("./session");
const cors = require("./cors");
const csrf = require("./csrf");
const cookieParser = require("./cookie-parser");
const bodyParser = require("./body-parser");
const validators = require("./validators");
const nodemailer = require("./nodemailer");

const router = Router();

router.use(cors);
router.use(bodyParser);
router.use(cookieParser);
router.use(session);
router.use(csrf);
router.use(auth);
router.use(validators);
router.use(nodemailer);

router.use((req, _, next) => {
  req.appUrl = req.protocol + "://" + req.get("host");
  next();
});

module.exports = router;
