import React from "react";
import App from "../components/layouts/App";
import Header from "../components/Header";
import Footer from "../components/Footer";
import Features from "../components/Features";
import Subscribe from "../components/Subscribe";
import Feeds from "../components/Feeds";
import Blog from "../components/Blog";
import Brands from "../components/Brands";
import TrendingItems from "../components/TrendingItems";
import Categories from "../components/Categories";
import Slider from "../components/Slider";
import CartSidebar from "../components/CartSidebar";
import MenusSidebar from "../components/MenusSidebar";

const Index = () => (
  <App>
    <div className="uk-offcanvas-content">
      <Header />
      <main>
        <Slider />
        <Categories />
        <TrendingItems />
        <Brands />
        <Blog />
        <Feeds />
        <Subscribe />
        <Features />
      </main>
      <Footer />
      <MenusSidebar />
      <CartSidebar />
    </div>
  </App>
);

export default Index;
