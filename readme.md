# GrandMart

Docker Containerized eCommerce Store using (NextJS,Express,Sequelite,JWT, ...etc)

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

- Git: for source control management

```bash
$ git version
```

- [Docker](https://docs.docker.com/install/) installed on your machine

```bash
$ docker version
```

### Installing

1. Clone this repo into your working directory

```bash
$ git clone https://gitlab.com/taekunger/grandmart.git
```

2. Run docker compose to download & build up your images

```bash
$ cd grandmart
$ docker-compose up -d
```

3. Run sequelize migrations

```bash
$ docker exec -it <node-container-id> bash
$ npx sequelize-cli db:migrate
```

4. Open new browser window & go to your website url [http://localhost:3000](http://localhost:3000)

5. Open new browser window & go to phpmyadmin url [http://localhost:8888](http://localhost:8888)

6. Make some changes and it will be reflected by hot-reload dev server

## Deployment

- [DigitalOcean](https://docs.docker.com/machine/examples/ocean/) - a Dockerized DigitalOcean Droplet
- [Others](https://docs.docker.com/v17.09/get-started/part6/) - Connect Docker Cloud

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/taekunger/grandmart/-/tags).

## Authors

- **Nady Shalaby** - _Initial work_ - [Taekunger](https://github.com/taekunger)

See also the list of [contributors](https://gitlab.com/taekunger/grandmart/-/graphs/master) who participated in this project.

## License

This project is licensed under the MIT License
