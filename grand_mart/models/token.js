"use strict";
module.exports = (sequelize, DataTypes) => {
  const Token = sequelize.define(
    "Token",
    {
      email: DataTypes.STRING,
      expiredAt: DataTypes.DATE,
      token: DataTypes.STRING,
      type: DataTypes.ENUM("email", "password")
    },
    {}
  );
  Token.associate = function(models) {
    // associations can be defined here
  };
  return Token;
};
