const { Router } = require("express");
const csrf = require("csurf");
const { parse } = require("url");
const { includes } = require("lodash");
const { NON_CSRF_ROUTES } = require("../config/config.json");

const router = Router();

router.use((req, res, next) => {
  if (includes(NON_CSRF_ROUTES, parse(req.url).pathname)) return next();
  return csrf({ cookie: true })(req, res, next);
});

router.use((req, res, next) => {
  if (req.csrfToken) {
    res.cookie("XSRF-TOKEN", req.csrfToken());
  }
  next();
});

module.exports = router;
