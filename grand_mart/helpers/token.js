const crs = require("crypto-random-string");
const moment = require("moment");
const jwt = require("jsonwebtoken");
const { Token, User } = require("../models");
const { JWT_SECRET } = require("../config/config.json");

/**
 * Generate unique email verification token record for a given user.
 *
 * @param {User} user
 * @returns {Token}
 */
const generateEmailVerificationForUser = async ({ email }) => {
  let randToken = "";
  let token = true;
  while (token) {
    randToken = crs({ length: 100 });
    token = await Token.findOne({
      where: { email, token: randToken, type: "email" },
      attributes: ["id"]
    });
  }

  return Token.create({
    type: "email",
    email,
    token: randToken,
    expiredAt: moment().add(1, "days")
  });
};

/**
 * Generate unique password reset token record for a given user
 * @param {User} user
 * @returns {Token}
 */

const generatePasswordResetForUser = async ({ email }) => {
  let randToken = "";
  let token = true;
  while (token) {
    randToken = crs({ length: 100 });
    token = await Token.findOne({
      where: { email, token: randToken, type: "password" },
      attributes: ["id"]
    });
  }

  return Token.create({
    type: "password",
    email,
    token: randToken,
    expiredAt: moment().add(1, "days")
  });
};

/**
 * Generate Auth JWT for a given user
 * @param {User} user
 * @returns {Promise}
 */

const generateJWTForUser = async ({ id, email }, options = {}) => {
  return Promise.resolve(jwt.sign({ id, email }, JWT_SECRET, options));
};

const verifyEmailByToken = async token => {
  const found = await Token.findOne({ where: { token, type: "email" } });

  if (!found || (found && moment().isAfter(found.expiredAt))) {
    return Promise.reject(
      new Error("Invalid Token!. May be expired or deleted")
    );
  }

  const updated = await User.update(
    { activatedAt: moment() },
    { where: { email: found.email } }
  );

  found.destroy();

  return Promise.resolve(!!updated);
};

module.exports = {
  generateEmailVerificationForUser,
  generatePasswordResetForUser,
  generateJWTForUser,
  verifyEmailByToken
};
