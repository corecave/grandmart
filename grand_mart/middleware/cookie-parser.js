const { Router } = require("express");
const cookieParser = require("cookie-parser");
const config = require("./../config/config.json");

const router = Router();

router.use(cookieParser(config.COOKIE_SECRET));

module.exports = router;
