import bcrypt from "bcrypt";
import { User } from "~/models";
import { generateEmailVerificationForUser } from "~/helpers/token";

export default (req, res) => {
  if (req.method === "POST") {
    const { name, email, password } = req.body;
    User.findOne({
      where: { email },
      attributes: ["id"]
    })
      .then(async user => {
        if (user) {
          res.status(422).json({ error: "This email already registered" });
          return;
        }

        const hash = await bcrypt.hash(password, 10);

        return User.create({ name, email, password: hash });
      })
      .then(user => {
        if (user) {
          generateEmailVerificationForUser(user).then(token => {
            var mail = {
              from: '"GrandMart" <no-reply@grandmart.com>',
              to: email,
              subject: "GrandMart. Email Verification",
              template: "email-verification",
              context: {
                name,
                home_url: req.appUrl,
                verification_url: `${req.appUrl}/auth/verify-email/${token.token}`
              }
            };

            req.mail.sendMail(mail);
          });

          return res.json({
            success:
              "Your account created successfully. We send you an email verification message to your inbox, please verify your email and login back"
          });
        }
      });
  } else {
    return res.status(400).json({ error: "400 Bad Request" });
  }
};

export const config = {
  api: {
    bodyParser: false
  }
};
