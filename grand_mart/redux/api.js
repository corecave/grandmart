import axios from "axios";
import Cookies from "js-cookie";

export const API_BASE = "http://localhost:3000/api";
export const API_REGISTER = "/auth/register";
export const API_LOGIN = "/auth/login";
export const API_VERIFY_EMAIL = "/auth/verify-email";

const http = axios.create({
  baseURL: API_BASE,
  timeout: 10000,
  headers: {
    "Content-Type": "application/json"
  }
});

http.interceptors.request.use(
  config => {
    if (process.browser) {
      config.headers["CSRF-Token"] =
        process.browser && Cookies.get("XSRF-TOKEN");
    }
    return config;
  },
  error => Promise.reject(error)
);

export default http;
