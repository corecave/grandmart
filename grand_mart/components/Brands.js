import React from "react";

function Brands() {
  return (
    <section className="uk-section uk-section-default uk-section-small">
      <div className="uk-container">
        <h2 className="uk-text-center">Popular Brands</h2>
        <div className="uk-margin-medium-top" uk-slider={{ finite: true }}>
          <div className="uk-position-relative">
            <div className="uk-grid-small uk-flex-middle" uk-grid="true">
              <div className="uk-visible@m">
                <a
                  href="#"
                  uk-slidenav-previous="true"
                  uk-slider-item="previous"
                ></a>
              </div>
              <div className="uk-width-expand uk-slider-container">
                <ul className="uk-slider-items uk-child-width-1-3 uk-child-width-1-6@s uk-grid uk-grid-large">
                  <li>
                    <div className="tm-ratio tm-ratio-16-9">
                      <a
                        className="uk-link-muted tm-media-box tm-grayscale"
                        href="#"
                        title="Apple"
                      >
                        <figure className="tm-media-box-wrap">
                          <img src="/images/brands/apple.svg" alt="Apple" />
                        </figure>
                      </a>
                    </div>
                  </li>
                  <li>
                    <div className="tm-ratio tm-ratio-16-9">
                      <a
                        className="uk-link-muted tm-media-box tm-grayscale"
                        href="#"
                        title="Samsung"
                      >
                        <figure className="tm-media-box-wrap">
                          <img src="/images/brands/samsung.svg" alt="Samsung" />
                        </figure>
                      </a>
                    </div>
                  </li>
                  <li>
                    <div className="tm-ratio tm-ratio-16-9">
                      <a
                        className="uk-link-muted tm-media-box tm-grayscale"
                        href="#"
                        title="Sony"
                      >
                        <figure className="tm-media-box-wrap">
                          <img src="/images/brands/sony.svg" alt="Sony" />
                        </figure>
                      </a>
                    </div>
                  </li>
                  <li>
                    <div className="tm-ratio tm-ratio-16-9">
                      <a
                        className="uk-link-muted tm-media-box tm-grayscale"
                        href="#"
                        title="Microsoft"
                      >
                        <figure className="tm-media-box-wrap">
                          <img
                            src="/images/brands/microsoft.svg"
                            alt="Microsoft"
                          />
                        </figure>
                      </a>
                    </div>
                  </li>
                  <li>
                    <div className="tm-ratio tm-ratio-16-9">
                      <a
                        className="uk-link-muted tm-media-box tm-grayscale"
                        href="#"
                        title="Intel"
                      >
                        <figure className="tm-media-box-wrap">
                          <img src="/images/brands/intel.svg" alt="Intel" />
                        </figure>
                      </a>
                    </div>
                  </li>
                  <li>
                    <div className="tm-ratio tm-ratio-16-9">
                      <a
                        className="uk-link-muted tm-media-box tm-grayscale"
                        href="#"
                        title="HP"
                      >
                        <figure className="tm-media-box-wrap">
                          <img src="/images/brands/hp.svg" alt="HP" />
                        </figure>
                      </a>
                    </div>
                  </li>
                  <li>
                    <div className="tm-ratio tm-ratio-16-9">
                      <a
                        className="uk-link-muted tm-media-box tm-grayscale"
                        href="#"
                        title="LG"
                      >
                        <figure className="tm-media-box-wrap">
                          <img src="/images/brands/lg.svg" alt="LG" />
                        </figure>
                      </a>
                    </div>
                  </li>
                  <li>
                    <div className="tm-ratio tm-ratio-16-9">
                      <a
                        className="uk-link-muted tm-media-box tm-grayscale"
                        href="#"
                        title="Lenovo"
                      >
                        <figure className="tm-media-box-wrap">
                          <img src="/images/brands/lenovo.svg" alt="Lenovo" />
                        </figure>
                      </a>
                    </div>
                  </li>
                  <li>
                    <div className="tm-ratio tm-ratio-16-9">
                      <a
                        className="uk-link-muted tm-media-box tm-grayscale"
                        href="#"
                        title="ASUS"
                      >
                        <figure className="tm-media-box-wrap">
                          <img src="/images/brands/asus.svg" alt="ASUS" />
                        </figure>
                      </a>
                    </div>
                  </li>
                  <li>
                    <div className="tm-ratio tm-ratio-16-9">
                      <a
                        className="uk-link-muted tm-media-box tm-grayscale"
                        href="#"
                        title="Acer"
                      >
                        <figure className="tm-media-box-wrap">
                          <img src="/images/brands/acer.svg" alt="Acer" />
                        </figure>
                      </a>
                    </div>
                  </li>
                  <li>
                    <div className="tm-ratio tm-ratio-16-9">
                      <a
                        className="uk-link-muted tm-media-box tm-grayscale"
                        href="#"
                        title="Dell"
                      >
                        <figure className="tm-media-box-wrap">
                          <img src="/images/brands/dell.svg" alt="Dell" />
                        </figure>
                      </a>
                    </div>
                  </li>
                  <li>
                    <div className="tm-ratio tm-ratio-16-9">
                      <a
                        className="uk-link-muted tm-media-box tm-grayscale"
                        href="#"
                        title="Canon"
                      >
                        <figure className="tm-media-box-wrap">
                          <img src="/images/brands/canon.svg" alt="Canon" />
                        </figure>
                      </a>
                    </div>
                  </li>
                </ul>
              </div>
              <div className="uk-visible@m">
                <a href="#" uk-slider-item="next" uk-slidenav-next="true"></a>
              </div>
            </div>
          </div>
          <ul className="uk-slider-nav uk-dotnav uk-flex-center uk-margin-medium-top uk-hidden@m"></ul>
        </div>
        <div className="uk-margin uk-text-center">
          <a
            className="uk-link-muted uk-text-uppercase tm-link-to-all"
            href="brands.html"
          >
            <span>see all brands</span>
            <span uk-icon="icon: chevron-right; ratio: .75;"></span>
          </a>
        </div>
      </div>
    </section>
  );
}

export default Brands;
