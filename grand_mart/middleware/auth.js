const { Router } = require("express");
const { parse } = require("url");
const jwt = require("jsonwebtoken");
const config = require("../config/config.json");
const { User } = require("../models");

const router = Router();
const authMiddleware = async (req, res, next) => {
  /*
   * Check if authorization header is set
   */
  let token = null;
  const { pathname } = parse(req.url);

  if (
    req.hasOwnProperty("session") &&
    req.session.hasOwnProperty("authorization")
  ) {
    token = req.session.authorization;
  } else if (
    req.hasOwnProperty("headers") &&
    req.headers.hasOwnProperty("authorization")
  ) {
    token = req.headers["authorization"];
  }

  if (token) {
    try {
      /*
       * Try to decode & verify the JWT token
       * The token contains user's id ( it can contain more informations )
       * and this is saved in req.user object
       */
      const decoded = jwt.verify(token, config.JWT_SECRET);
      req.user = User.findOne({ where: { email: decoded.email } });

      if (!req.user) {
        throw new Error();
      }
    } catch (err) {
      /*
       * If the authorization header is corrupted, it throws exception
       * So return 401 status code with JSON error message
       */
      if (pathname.startsWith("/api")) {
        res.status(401).json({
          error: {
            msg: "Failed to authenticate token!"
          }
        });
      } else {
        return res.status(401).redirect("back");
      }
    }
  } else {
    /*
     * If there is no autorization header, return 401 status code with JSON
     * error message
     */
    if (pathname.startsWith("/api")) {
      return res.status(401).json({
        error: {
          msg: "Failed to authenticate token!"
        }
      });
    } else {
      return res.status(401).redirect("back");
    }
  }
  next();
  return;
};

const privateRoutes = config.PRIVATE_ROUTES;

/**
 * Connecting private routes with authMiddleware
 */
for (let route = 0; route < privateRoutes.length; route++) {
  const privateRoute = privateRoutes[route];
  router.use(privateRoute, authMiddleware);
}

module.exports = router;
