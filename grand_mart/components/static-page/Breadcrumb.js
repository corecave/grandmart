import React from "react";
import A from "next/link";
import PropTypes from "prop-types";

function Breadcrumb({ title, hasTitle }) {
  return (
    <div className="uk-text-center">
      <ul className="uk-breadcrumb uk-flex-center uk-margin-remove">
        <li>
          <A href="/">
            <a>Home</a>
          </A>
        </li>
        <li>
          <span>{title}</span>
        </li>
      </ul>
      {hasTitle && (
        <h1 className="uk-margin-small-top uk-margin-remove-bottom">{title}</h1>
      )}
    </div>
  );
}

Breadcrumb.propTypes = {
  title: PropTypes.string.isRequired,
  hasTitle: PropTypes.bool
};

export default Breadcrumb;
