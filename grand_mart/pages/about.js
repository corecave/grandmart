import React from "react";
import StaticPage from "../components/layouts/StaticPage";

const About = () => (
  <StaticPage title="About">
    <section>
      <div>
        <article className="uk-card uk-card-default uk-card-body uk-article tm-ignore-container">
          <header className="uk-text-center">
            <h1 className="uk-article-title">About</h1>
          </header>
          <div className="uk-article-body">
            <p className="uk-text-lead uk-text-center">
              Urabitur justo diam, auctor vitae ornare sit amet, accumsan sed
              neque. Curabitur efficitur lacinia euismod. Nunc dictum sagittis
              lacus. Etiam ultrices nulla orci, in ultrices risus.
            </p>
            <p>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas
              ac tortor sit amet nisi malesuada commodo. Phasellus et tempus
              justo. Sed iaculis dignissim lacinia. Nulla id felis vel ligula
              tempus sodales vel a ante. Lorem ipsum dolor sit amet, consectetur
              adipiscing elit. Maecenas quis neque ac elit lacinia laoreet. Sed
              dolor sem, rutrum ac egestas non, tempor nec eros. Etiam lobortis
              porta viverra. Etiam ut suscipit sem, a volutpat mi. Maecenas
              euismod a lectus ut dapibus. Nulla mattis diam et leo lacinia
              dignissim.
            </p>
            <h2 className="uk-text-center">Our principles</h2>
            <ul className="uk-list uk-list-bullet">
              <li>
                Vestibulum ut mollis est. Fusce iaculis mauris ut tortor
                convallis sollicitudin. Suspendisse porta nulla nibh, id lacinia
                lacus tempus ut. Morbi non arcu aliquam, placerat sapien a,
                luctus diam. Etiam mattis cursus sem, eu maximus nisi bibendum
                nec. Vivamus ut turpis augue. Phasellus vehicula risus sit amet
                mi luctus malesuada.
              </li>
              <li>
                Curabitur justo diam, auctor vitae ornare sit amet, accumsan sed
                neque. Curabitur efficitur lacinia euismod. Nunc dictum sagittis
                lacus. Etiam ultrices nulla orci, in ultrices risus tincidunt
                ac. Cras et maximus mauris. Morbi aliquam efficitur maximus.
                Aenean orci diam, auctor a mattis eu, consectetur id urna.
              </li>
              <li>
                Morbi faucibus mattis ante. Donec varius neque sem, nec
                convallis mi dictum ut. Duis sit amet massa ac eros luctus
                egestas. Proin hendrerit aliquam metus, ac tincidunt risus
                viverra at. In viverra, ligula in facilisis interdum, dui arcu
                varius purus, eu blandit mi mi ut diam. Phasellus finibus metus
                sit amet lobortis dapibus. Nunc fringilla ac erat vitae
                elementum. Donec sagittis odio non mi vestibulum accumsan.
              </li>
            </ul>
            <h2 className="uk-text-center">Our team</h2>
            <div
              className="uk-child-width-1-1 uk-child-width-1-2@s uk-child-width-1-3@m"
              uk-grid=""
            >
              <div>
                <div className="uk-grid-small uk-flex-middle" uk-grid="">
                  <div>
                    <img
                      src="images/about/thomas.svg"
                      alt="Thomas Bruns"
                      width={80}
                      height={80}
                    />
                  </div>
                  <div className="uk-width-expand">
                    <div>Thomas Bruns</div>
                    <div className="uk-text-meta">Co-founder &amp; CEO</div>
                  </div>
                </div>
              </div>
              <div>
                <div className="uk-grid-small uk-flex-middle" uk-grid="">
                  <div>
                    <img
                      src="images/about/george.svg"
                      alt="George Clanton"
                      width={80}
                      height={80}
                    />
                  </div>
                  <div className="uk-width-expand">
                    <div>George Clanton</div>
                    <div className="uk-text-meta">
                      Co-founder &amp; President
                    </div>
                  </div>
                </div>
              </div>
              <div>
                <div className="uk-grid-small uk-flex-middle" uk-grid="">
                  <div>
                    <img
                      src="images/about/martin.svg"
                      alt="Martin Cade"
                      width={80}
                      height={80}
                    />
                  </div>
                  <div className="uk-width-expand">
                    <div>Martin Cade</div>
                    <div className="uk-text-meta">Co-founder &amp; CTO</div>
                  </div>
                </div>
              </div>
              <div>
                <div className="uk-grid-small uk-flex-middle" uk-grid="">
                  <div>
                    <img
                      src="images/about/carol.svg"
                      alt="Carol Issa"
                      width={80}
                      height={80}
                    />
                  </div>
                  <div className="uk-width-expand">
                    <div>Carol Issa</div>
                    <div className="uk-text-meta">
                      Former Commercial Director
                    </div>
                  </div>
                </div>
              </div>
              <div>
                <div className="uk-grid-small uk-flex-middle" uk-grid="">
                  <div>
                    <img
                      src="images/about/patricia.svg"
                      alt="Patricia Kirk"
                      width={80}
                      height={80}
                    />
                  </div>
                  <div className="uk-width-expand">
                    <div>Patricia Kirk</div>
                    <div className="uk-text-meta">
                      Former Director of Strategy
                    </div>
                  </div>
                </div>
              </div>
              <div>
                <div className="uk-grid-small uk-flex-middle" uk-grid="">
                  <div>
                    <img
                      src="images/about/nicole.svg"
                      alt="Nicole Yokoyama"
                      width={80}
                      height={80}
                    />
                  </div>
                  <div className="uk-width-expand">
                    <div>Nicole Yokoyama</div>
                    <div className="uk-text-meta">
                      Product Marketing Manager
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <h2 className="uk-text-center">Some stats</h2>
            <div
              className="uk-child-width-1-1 uk-child-width-1-3@s uk-text-center"
              uk-grid=""
            >
              <div>
                <div className="uk-heading-primary uk-text-warning">5+</div>
                <div className="uk-margin-small-top">years on the market</div>
              </div>
              <div>
                <div className="uk-heading-primary uk-text-warning">150+</div>
                <div className="uk-margin-small-top">orders per day</div>
              </div>
              <div>
                <div className="uk-heading-primary uk-text-warning">75000+</div>
                <div className="uk-margin-small-top">clients</div>
              </div>
            </div>
            <h2 className="uk-text-center">Store</h2>
            <p>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus
              imperdiet venenatis est. Phasellus vitae mauris imperdiet,
              condimentum eros vel, ullamcorper turpis. Maecenas sed libero quis
              orci egestas vehicula fermentum id diam.
            </p>
            <figure>
              <div className="uk-text-bolder">Store Name</div>
              <div>St.&nbsp;Petersburg, Nevsky&nbsp;Prospect&nbsp;28</div>
              <div>Daily 10:00–22:00</div>
            </figure>
            <div className="tm-wrapper">
              <figure
                className="tm-ratio tm-ratio-16-9 js-map"
                data-latitude="59.9356728"
                data-longitude="30.3258604"
                data-zoom={14}
              />
            </div>
          </div>
        </article>
      </div>
    </section>
  </StaticPage>
);

export default About;
