import { CHANGE_LOGIN_FIELD, CHANGE_REGISTER_FIELD } from "./../constants";

const initialState = {
  login: {
    email: "",
    password: "",
    rememberMe: true
  },
  register: {
    name: "",
    email: "",
    password: "",
    passwordAgain: ""
  }
};

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case CHANGE_LOGIN_FIELD:
      return { ...state, login: { ...state.login, ...payload } };
    case CHANGE_REGISTER_FIELD:
      return { ...state, register: { ...state.register, ...payload } };
    default:
      return state;
  }
};
