import React from "react";

function Features() {
  return (
    <section className="uk-section uk-section-default uk-section-small">
      <div className="uk-container">
        <div uk-slider="true">
          <ul className="uk-slider-items uk-child-width-1-1 uk-child-width-1-2@s uk-child-width-1-5@m uk-grid">
            <li>
              <div
                className="uk-grid-small uk-flex-center uk-flex-left@s"
                uk-grid="true"
              >
                <div>
                  <span uk-icon="icon: star; ratio: 2.5;"></span>
                </div>
                <div className="uk-text-center uk-text-left@s uk-width-expand@s">
                  <div>Mauris placerat</div>
                  <div className="uk-text-meta">
                    Donec mollis nibh dolor, sit amet auctor
                  </div>
                </div>
              </div>
            </li>
            <li>
              <div
                className="uk-grid-small uk-flex-center uk-flex-left@s"
                uk-grid="true"
              >
                <div>
                  <span uk-icon="icon: receiver; ratio: 2.5;"></span>
                </div>
                <div className="uk-text-center uk-text-left@s uk-width-expand@s">
                  <div>Lorem ipsum</div>
                  <div className="uk-text-meta">
                    Sit amet, consectetur adipiscing elit
                  </div>
                </div>
              </div>
            </li>
            <li>
              <div
                className="uk-grid-small uk-flex-center uk-flex-left@s"
                uk-grid="true"
              >
                <div>
                  <span uk-icon="icon: location; ratio: 2.5;"></span>
                </div>
                <div className="uk-text-center uk-text-left@s uk-width-expand@s">
                  <div>Proin pharetra</div>
                  <div className="uk-text-meta">
                    Nec quam a fermentum ut viverra
                  </div>
                </div>
              </div>
            </li>
            <li>
              <div
                className="uk-grid-small uk-flex-center uk-flex-left@s"
                uk-grid="true"
              >
                <div>
                  <span uk-icon="icon: comments; ratio: 2.5;"></span>
                </div>
                <div className="uk-text-center uk-text-left@s uk-width-expand@s">
                  <div>Praesent ultrices</div>
                  <div className="uk-text-meta">
                    Praesent ultrices, orci nec finibus
                  </div>
                </div>
              </div>
            </li>
            <li>
              <div
                className="uk-grid-small uk-flex-center uk-flex-left@s"
                uk-grid="true"
              >
                <div>
                  <span uk-icon="icon: happy; ratio: 2.5;"></span>
                </div>
                <div className="uk-text-center uk-text-left@s uk-width-expand@s">
                  <div>Duis condimentum</div>
                  <div className="uk-text-meta">
                    Pellentesque eget varius arcu
                  </div>
                </div>
              </div>
            </li>
          </ul>
          <ul className="uk-slider-nav uk-dotnav uk-flex-center uk-margin-medium-top"></ul>
        </div>
      </div>
    </section>
  );
}

export default Features;
